#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>


/*total number of lines, words, chars (if multiple files are input)*/
static int total_lines = 0, total_words = 0, total_chars = 0;
void wc(FILE *infile, char *inname);

int main(int argc, char *argv[]) {
    
        FILE *fileptr;
    if(argc > 1) {
 
    for(int p=1;p<argc;p++)
        {
        // TODO: get the filename/s from argv, open each and pass it to wc
        /*  open the file for reading */
        fileptr = fopen(argv[p], "r");
        wc(fileptr,argv[p]);

        }
    }
        
        if(argc > 2) { // print totals for multiple files
    
            printf("%7d %7d %7d %s\n",  total_lines, total_words, total_chars, "total");
        
        } else {
        // TODO: no arguments, process input from stdin

                       char filename[25];
                       scanf("%s",filename);

                       if(access(filename,F_OK)!=-1){
                        fileptr = fopen(filename, "r");
                        wc(fileptr,filename);

                    }else
                    {
                        printf("%s\n","File does not exist , please try again");
                        scanf("%s",filename);

                       if(access(filename,F_OK)!=-1)
                        fileptr = fopen(filename, "r");
                        wc(fileptr,filename);
                    }
                }
        
    if (fileptr == NULL){

        printf("Cannot open file \n");

        exit(0);}
        
    return 0;
}

void wc(FILE *infile, char *inname) {
    int lines = 0, words = 0, chars = 0;
    
    // TODO: read the file character by character and count the
    //       number of lines, words, and characters.
        int check=1;
      char  contents;

      while ((contents=fgetc(infile)) != EOF){

        chars++;         

        if (contents == '\n')
        lines++;     

        if (isspace(contents)){
        check=1; 

        }else{
            words+=check;
        check=0;
        }

    }
    
    total_lines += lines;
    total_words += words;
    total_chars += chars;
    printf("%7d %7d %7d %s\n",  lines, words, chars, inname);

}