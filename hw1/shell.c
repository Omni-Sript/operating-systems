#define _POSIX_SOURCE

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdbool.h>
#include <sys/stat.h>

#define INPUT_STRING_SIZE 80
#define CNORMAL "\x1B[0m"
#define CYELLOW "\x1B[33m"

#include "io.h"
#include "parse.h"
#include "process.h"
#include "shell.h"

int cmd_help(tok_t arg[]);
int cmd_quit(tok_t arg[]);
int cmd_pwd();
int cmd_cd(tok_t arg[]);
int cmd_fg(tok_t arg[]);
int cmd_wait(tok_t arg[]);


char size[1000];
char prevdir[1000];
pid_t childPID;
 
/** 
 *  Built-In Command Lookup Table Structures 
 */
typedef int cmd_fun_t (tok_t args[]); // cmd functions take token array and return int
typedef struct fun_desc {
    cmd_fun_t *fun;
    char *cmd;
    char *doc;
} fun_desc_t;
/** TODO: add more commands (pwd etc.) to this lookup table! */
fun_desc_t cmd_table[] = {
    {cmd_help, "help", "show this help menu"},
    {cmd_quit, "quit", "quit the command shell"},
    {cmd_pwd, "pwd","Print current working directory"},
    {cmd_cd,"cd","Change Current Directory"},
    {cmd_fg,"fg","Put process in foreground"},
    {cmd_wait,"wait","wait for all process to terminate"},

};

/**
 *  Determine whether cmd is a built-in shell command
 *
 */
int lookup(char cmd[]) {
    unsigned int i;
    for (i=0; i < (sizeof(cmd_table)/sizeof(fun_desc_t)); i++) {
        if (cmd && (strcmp(cmd_table[i].cmd, cmd) == 0)) return i;
    }
    return -1;
}

/*
 *  Print the help table (cmd_table) for built-in shell commands
 */
int cmd_help(tok_t arg[]) {
    unsigned int i;
    for (i = 0; i < (sizeof(cmd_table)/sizeof(fun_desc_t)); i++) {
        printf("%s - %s\n",cmd_table[i].cmd, cmd_table[i].doc);
    }
    return 1;
}

int cmd_fg(tok_t arg[]){
    if(!arg[0]){
        if(first_process==NULL){
            printf("no process to put in the foreground");
        }
        else if(first_process->next==NULL){
            if(first_process->completed==false){
                put_process_in_foreground(first_process,1);
                first_process->completed=true;
            }
        }
        else{
            while(first_process->next!=NULL){
              first_process=first_process->next;  
            }
            put_process_in_foreground(first_process,1);
        }
    }

    else{
        pid_t fgPID = (pid_t)atoi(arg[0]);

        //PROCEDURE TO VALIDATE PROCESS
        if(first_process==NULL){
             printf("no process to put in the foreground");
        }
        else if(first_process->pid==fgPID){
                put_process_in_foreground(first_process,1);
                first_process->completed=true;
                first_process = first_process->next;
        }
        else{
            while(first_process->next!=NULL){
        
              if(first_process->pid==fgPID){
                put_process_in_foreground(first_process,1);
                first_process->completed=true;
                first_process = first_process->next;
              }
             first_process=first_process->next;
            }
            
        }

    }
return 1;

}

int cmd_wait(tok_t arg[]){
    if(first_process==NULL){
     // no process in background to wait for

    }else if(first_process->next==NULL){
            if((first_process->background)==true){
                put_process_in_foreground(first_process,1);
                first_process->completed=true;
                first_process->background=false;
            }

        }else{
            while(first_process->next!=NULL){

              if((first_process->background)==true){
                put_process_in_foreground(first_process,1);
                first_process->completed=true;
                first_process->background=false;
              }  
              first_process=first_process->next;
            }
            
        }
    return 1;
}

int cmd_cd(tok_t arg[]) {
// ~ implementation needs to work in a loop
//integrated with other functions
    char* NewDir = (char*)arg[0];
    int check;
    char* home = getenv("HOME");

    if(strcmp(NewDir,"~")==0){   
        getcwd(prevdir,sizeof(prevdir));
        chdir(home);        
    }else if(strcmp(NewDir,"-")==0){
        chdir(prevdir);
    //NEED TO FIND A PROPER CONDITION
    }else{
        char* x = strtok(arg[0],"~");
        char* home1 = (char*)malloc(1250);
        strcpy(home1,home);
        strcat(home1,x);
        //printf("%s\n", home1);
        getcwd(prevdir,sizeof(prevdir));
        //printf("%s\n", x);
        if((check = chdir(home1)) != 0){
        getcwd(prevdir,sizeof(prevdir)); 
        chdir(NewDir);    
      }
        x = strtok(arg[0],"/");
        //printf("%s\n", x);  
    }
                     
    return 0;

}

int cmd_pwd(){

    char size[1000];
    getcwd(size, sizeof(size));
    printf("%s\n", size);
        return 1;
}

/**
 *  Quit the terminal
 *
 */
int cmd_quit(tok_t arg[]) {
    printf("Bye\n");
    exit(0);
    return 1;
}

/**
 *  Initialise the shell
 *
 */
void init_shell() {
    // Check if we are running interactively
    shell_terminal = STDIN_FILENO;

    // Note that we cannot take control of the terminal if the shell is not interactive
    shell_is_interactive = isatty(shell_terminal);

    if( shell_is_interactive ) {

        // force into foreground
        while(tcgetpgrp (shell_terminal) != (shell_pgid = getpgrp()))
            kill( - shell_pgid, SIGTTIN);
        shell_pgid = getpid();
        // Put shell in its own process group
        if(setpgid(shell_pgid, shell_pgid) < 0){
            perror("Couldn't put the shell in its own process group");
            exit(1);
        }
        // Take control of the terminal
        tcsetpgrp(shell_terminal, shell_pgid);
        tcgetattr(shell_terminal, &shell_tmodes);
    }

    /** TODO */
    // ignore signals
    signal(SIGCONT,SIG_IGN);
    signal(SIGTTOU,SIG_IGN);
    signal(SIGINT,SIG_IGN);
    signal(SIGTSTP,SIG_IGN);
    signal(SIGTTIN,SIG_IGN);
    signal(SIGTERM,SIG_IGN);

    
}
/*
 * Add a process to our process list
 */
void add_process(process* p)
{
    /** TODO **/
    if(first_process==NULL){
        first_process=p;
    }
    else{
         while(first_process->next != NULL){
            first_process=first_process->next;
        }
        first_process->next = p;
        p->prev = first_process;
        p->next = NULL;
    }
}


/* Creates a process given the tokens parsed from stdin
 */
process* create_process(tok_t* tokens)
{
    /** TODO **/

    process* takeInput =(process*)malloc(sizeof(process));
    takeInput->argv = tokens;
    int count = 0;
    while(tokens[count] != NULL){
        count++;
    }
    takeInput->argc = count;
    return takeInput;

}

process* intialise_process(tok_t* tokens){
    process* Process=first_process;

    Process->argv= tokens;
    Process->pid=tcsetpgrp(STDOUT_FILENO, getpid());
    Process->completed=true;
    Process->background=false;

  return Process;
}


/*
 * Main shell loop
 */
int shell (int argc, char *argv[]) {
    int lineNum = 0;
    pid_t pid = getpid();   // get current process's PID
    pid_t ppid = getppid(); // get parent's PID
    pid_t cpid;             // use this to store a child PID

    char *s = malloc(INPUT_STRING_SIZE+1); // user input string
    tok_t *t;                              // tokens parsed from input
    // if you look in parse.h, you'll see that tokens are just C-style strings (char*)
    // perform some initialisation
    init_shell();
    //char size[1000];
    getcwd(size, sizeof(size));
    getcwd(prevdir, sizeof(prevdir));


    fprintf(stdout, "%s running as PID %d under %d\n",argv[0],pid,ppid);
    /** TODO: replace "TODO" with the current working directory */
    fprintf(stdout, CYELLOW "\n%d %s# " CNORMAL, lineNum, size);
    
    // Read input from user, tokenize it, and perform commands
    while ( ( s = freadln(stdin) ) ) { 
    
        t = getToks(s);            // break the line into tokens
        int fundex = lookup(t[0]); // is first token a built-in command?
        if( fundex >= 0 ) {
            cmd_table[fundex].fun(&t[1]); // execute built-in command
        } else {
            /** TODO: replace this statement to call a function that runs executables */
            process *TheProcess = create_process(t);
            add_process(TheProcess);
            launch_process(TheProcess);  
        }
        lineNum++;
        /** TODO: replace "TODO" with the current working directory */
        getcwd(size, sizeof(size));
        fprintf(stdout, CYELLOW "\n%d %s# " CNORMAL, lineNum, size);
    }
    return 0;}

