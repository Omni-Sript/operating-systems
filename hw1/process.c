#define _POSIX_SOURCE

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdbool.h>
#include <sys/stat.h>

#define INPUT_STRING_SIZE 80
#define CNORMAL "\x1B[0m"
#define CYELLOW "\x1B[33m"

#include "io.h"
#include "parse.h"
#include "process.h"
#include "shell.h"
#include <stddef.h>
/**
 *  Executes the process p.
 *  If the shell is in interactive mode and the process is a foreground process,
 *  then p should take control of the terminal.
 *
 */

//Function to find and change dirrectries and run execv
void run_executable(char* seperatedPath,char* app,process* p, bool found){

while((seperatedPath) != NULL){
            found = true;
            char* executable = (char*)malloc(1250);
            //copy seperated path into executable;
            strcpy(executable,seperatedPath);
            //append / to seperated path before running the 
            strcat(executable,"/");
            //append the executable to the seperated path before running exev
            strcat(executable,app);

            if((execv(executable,p->argv))==-1){
                if(execv(app,p->argv)==-1){
                    found=false;
                }
            }
            seperatedPath = strtok(NULL,":");
        }
        if(found==false){
            printf("Could not execute'%s': No such file or directory\n",p->argv[0]);
        }
}


void launch_process(process *p) {
    /** TODO **/
    pid_t childPID;
    char* redirect = (char*)malloc(1250);
    char* filename;
    int count = 0;
    char* path = getenv("PATH");
    childPID = fork();
    char* app = (char*)p->argv[0];
    char* executable = (char*)malloc(1250);
    char* seperatedPath;
    bool found = false;
    char* checkOUT = ">";
    char* checkIN = "<";
    int l,m,k,signalNO;

    /* IGNORE ALL THERESE IN THE PARENT PROCESS:
    Update your shell to ignore SIGINT, SIGTSTP, SIGTTIN, SIGTTOU.
    HANDLE THESE TWO SIGNALS IN THE CHOD PROCESS:
    If SIGINT or SIGTSTP is received, your shell should send the signal to the spawned subprocesses instead.*/
    while(p->argv[count] != NULL){
        count++;
    }

    if (childPID == 0){
        // IN CHILD PROCESS
        setpgid(0,0);
        //SIGNAL HANDLING
        signal(SIGTERM,SIG_DFL);
        signal(SIGINT,SIG_DFL);
        signal(SIGTSTP,SIG_DFL);
        signal(SIGQUIT,SIG_DFL);
        signal(SIGCONT,SIG_DFL);
        signal(SIGTTIN,SIG_DFL);
        signal(SIGTTOU,SIG_DFL);
        //PATH RESOLURION AND EXECUTABLES
        char* pathlist= (char*)malloc(1250);
        strcpy(pathlist,path);
        seperatedPath = strtok(pathlist, ":");

        for(k=0;k<MAXTOKS && p->argv[k]!=NULL;k++){
             if((strcmp(p->argv[k],">"))==0)
            {   
                if(k<(int)sizeof(p->argv)){
                    filename =p->argv[k+1];
                }
                p->argv[k]=NULL;
                
                int getfile = open(filename,O_CREAT|O_APPEND|O_RDWR,0600);      
                dup2(getfile,1);
                run_executable(seperatedPath,app,p,found);  
            }
        }

        for(m=0;m<MAXTOKS && p->argv[m]!=NULL;m++){
             if((strcmp(p->argv[m],"<"))==0)
            {
                if(m<(int)sizeof(p->argv)){
                    filename = p->argv[m+1];
                }
                p->argv[m]=NULL;
                
                int getfile = open(filename,O_RDWR,0600);      
                dup2(getfile,0); 
                run_executable(seperatedPath,app,p,found);  
            }
        }

        if((strcmp(p->argv[p->argc-1],"&"))==0){
            p->argv[p->argc-1]=NULL;
            run_executable(seperatedPath,app,p,found);   
        }
        else
        {
            run_executable(seperatedPath,app,p,found);           
        } 

    }
    else
    {  // IN PARENT PROCESS
        p->pid = childPID;
        //printf(" childPID is : %i\n",childPID);
        if(strcmp(p->argv[count-1],"&") == 0){
            p->background=true;
            put_process_in_background(p,1);
        }
        else{
            put_process_in_foreground(p,0);
            p->completed=true;
        }
    }

}

/**
 *  Put a process in the foreground. This function assumes that the shell
 *  is in interactive mode. If the cont argument is true, send the process
 *  group a SIGCONT signal to wake it up.
 *
 */
void put_process_in_foreground (process *p, int cont) {
    /** TODO **/
    tcsetpgrp(shell_terminal,p->pid);
    int status;
    if(cont){
        p->status = status;
        p->completed =true;
        if(kill(p->pid,SIGCONT) < 0){
            perror("SIGCONT failed");
        }
    }

    do {
        waitpid(p->pid, &status, WUNTRACED);
        if(WIFSTOPPED(status) == 0){
            break;
        }
    } while(WIFSTOPPED(status) == 0);

    tcsetpgrp(shell_terminal,shell_pgid);
    tcgetattr(shell_terminal,&p->tmodes);
    tcsetattr(shell_terminal,TCSADRAIN,&shell_tmodes);
}

/**
 *  Put a process in the background. If the cont argument is true, send
 *  the process group a SIGCONT signal to wake it up. 
 *
 */
void put_process_in_background (process *p, int cont) {
    /** TODO **/
    if(cont){
        int sending= kill(p->pid,SIGCONT);
        p->background = true;

    }
}

/**
 *  Prints the list of processes.
 *
 */
void print_process_list(void) {
    process* curr = first_process;
    while(curr) {
        if(!curr->completed) {
            printf("\n");
            printf("PID: %d\n",curr->pid);
            printf("Name: %s\n",curr->argv[0]);
            printf("Status: %d\n",curr->status);
            printf("Completed: %s\n",(curr->completed)?"true":"false");
            printf("Stopped: %s\n",(curr->stopped)?"true":"false");
            printf("Background: %s\n",(curr->background)?"true":"false");
            printf("Prev: %lx\n",(unsigned long)curr->prev);
            printf("Next: %lx\n",(unsigned long)curr->next);
        }
        curr=curr->next;
    }
}